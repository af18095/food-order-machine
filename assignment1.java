import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class assignment1 {
    private JPanel root;
    private JButton udonButton;
    private JButton kurageButton;
    private JButton ramenButton;
    private JButton tempuraButton1;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton checkOutButton;
    private JTextArea textArea1;
    private JLabel total;
    private int cost =0;

    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null ,
                "would you like to order " + food +"?",
                "order confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0){
            JOptionPane.showMessageDialog (null ,"order for "+food+" received");
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+food+"! It will be served as soon as possible.");
            cost += 100;
            total.setText("Total "+cost+ " yen ");

        }

    }


    public assignment1() {
        tempuraButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempura");
                textArea1.append("tempura\n");

            }
        });
        kurageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("kurage");
                textArea1.append("kurage\n");

            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyoza");
                textArea1.append("gyoza");

            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("udon");
                textArea1.append("udon\n");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("yakisoba");
                textArea1.append("yakisoba\n");
            }
        });

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ramen");
                textArea1.append("ramen \n");

            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null ,
                        "would you like to confirmation",
                        "checkout confirmation",

                        JOptionPane.YES_NO_OPTION);

            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+cost+" yen.");
                    cost = 0;
                    textArea1.setText("");
                    total.setText("Total   "+cost+" yen");

                }

            }
        });

    }public static void main (String[] args){
        JFrame frame = new JFrame("assignment1");
        frame.setContentPane(new assignment1().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
